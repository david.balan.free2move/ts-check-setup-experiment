# ts-check-setup-experiment

> This code has various bugs, this not the point of this project

This is a very simple 2D game written in JS for Node.js. It uses varios kind of data:

- types arrays (generics)
- functions
- classes
- interfaces
- primitives

By following the various branches, you'll understand how we're typing all those things.

Go to the branch `step1` to learn more : `git checkout step1`.