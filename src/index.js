import getScene from './graphics/Scene.js';
import Vec2 from './graphics/Vec2.js';
import Character from './graphics/Character.js';
import sleep from './tools/sleep.js';
import ms from 'ms';
import keyboard from './tools/keyboard.js';

const FPS = 5;

function render(renderer) {
    console.clear();
    console.log(renderer.map(row => row.join('')).join('\n'));
}

async function main() {
    const size = new Vec2({ x: 20, y: 8 });
    const pnj = new Character({ position: new Vec2({ x: 4, y: 2 }) });
    const player = new Character({ position: new Vec2({ x: 10, y: 7 }), character: '🦸' });
    keyboard('up', () => { player.position.y-= 1; });
    keyboard('down', () => { player.position.y+= 1; });
    keyboard('right', () => { player.position.x+= 1; });
    keyboard('left', () => { player.position.x-= 1; });
    let running = true;
    while (running) {
        const scene = getScene(size);
        pnj.render(scene);
        player.render(scene);
        render(scene);
        await sleep(ms('1s') / FPS);
    }
}

main().catch(e => { console.error(e); process.exit(2); });