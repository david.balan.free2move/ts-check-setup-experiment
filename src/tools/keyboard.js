import readline from 'readline';

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

const eventMap = new Map();

process.stdin.on('keypress', (str, key) => {
    if (key.ctrl && key.name === 'c') {
        process.exit();
    } else {
        if(eventMap.has(key.name)) {
            [...eventMap.get(key.name)].forEach(handler => handler());
        }
    }
});

export default (key, handler) => {
    if(!eventMap.has(key)) {
        eventMap.set(key, new Set());
    }
    eventMap.get(key).add(handler);
    return () => {
        eventMap.get(key).remove(handler);
        if(eventMap.get(key).size) {
            return eventMap.delete(key);
        }
    }
}