export default class Vec2 {
    x;
    y;
    constructor(options) {
        const { x, y } = options;
        this.x = x;
        this.y = y;
    }

    add(vector) {
        if (typeof vector === 'number') {
            return new Vec2({
                x: this.x + vector,
                y: this.y + vector
            });
        }
        return new Vec2({
            x: this.x + vector.x,
            y: this.y + vector.y
        });
    }

    dist(vector) {
        return Vec2.dist(this, vector);
    }

    static dist(vector1, vector2) {
        return Math.sqrt((vector1.x - vector2.x) ** 2 + (vector1.y - vector2.y) ** 2);
    }
}