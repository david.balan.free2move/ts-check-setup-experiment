export default function getScene (size) {
    return Array(size.y).fill([]).map(() => '⬛'.repeat(size.x).split(''));
}