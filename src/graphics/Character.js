export default class Character {
    position;
    character;
    constructor(options) {
        this.position = options.position;
        this.character = options.character || '😈';
    }

    render (renderer) {
        renderer[this.position.y - 1][this.position.x - 1] = this.character;
    }
}